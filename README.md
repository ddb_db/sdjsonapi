# OpenAPI Spec for Schedules Direct JSON Service
An unofficial OpenAPI spec for the [SDJSON](https://schedulesdirect.org) service.
Use this spec to generate clients in any programming language that has tooling to
generate OpenAPI clients from a spec file.

## Work In Progress
The current spec file build is **not complete**.  It is a work in progress and will
be updated as the underlying project I'm working on makes progress.  I welcome any
and all help via pull requests to enhance this spec to more thoroughly define the
SDJSON API.

When I feel there is a version of this spec that is sufficiently complete, I will
tag it and make a "release" of this project.  Until such a release is tagged, use
of the current spec file should be considered experimental.

## Grab the Built Spec File
The `openapi.yaml` file at the root of this project is **NOT** the complete spec
file.  This file references the various yaml files in the subdirectories.  Some
tools might be able to dereference and piece together the complete spec while
other tools will not.  Either be sure to grab all of the subdirs alongside the
root yaml file or grab the built spec file, which is a single combined spec file
built using [redocly](https://redocly.com/).

Each commit to `main` is built and published to the project's [repository](https://gitlab.com/ddb_db/sdjsonapi/-/packages).
Any such build should be considered experimental and may or may not work.  Only
tagged versions found in project [releases](https://gitlab.com/ddb_db/sdjsonapi/-/releases)
should be considered stable.

## Supported Programming Languages
My underlying project that is using this data is being written in Go so I'm using
the spec file to generate a Go based client for accessing SDJSON.  I'm using the
[oapi-codegen](https://github.com/deepmap/oapi-codegen) tool to generate my client
from the spec file I'm creating here.  This is how I'm testing the spec file for
correctness (either my client can be built from the spec or it can't).  If you've
used other tools in other languages to build a client from this spec file then
please let me know so it can be added to the list.